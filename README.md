# Yellow Media

Test project

## Requirements

To run the application you need to have:
- installed and running Docker.

## Up & running

- clone this repository.
- cd to the cloned project directory.
- run: docker-compose up -d.
- after container is started.
- run: docker-compose exec --user=1000 app bash.

- in the app container console run:
- composer install --optimize-autoloader --no-cache.
- cp .env.example .env
- php artisan migrate.

## Testing

- Postman collection - https://www.getpostman.com/collections/b7c072db118fa314c484
- Default environment file find in base path of this repository