<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'password_reminders',
            function (Blueprint $table) {
                $table->bigIncrements('id');
                
                $table->bigInteger('user_id')->index()->unsigned();
                
                $table->string('code')->index();
                $table->boolean('completed')->default(0);
                
                $table->timestamp('completed_at')->nullable();
                
                $table->timestamps();
                
                $table->foreign('user_id')->references('id')->on('users')
                    ->onUpdate('cascade')->onDelete('cascade');
            }
        );
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
