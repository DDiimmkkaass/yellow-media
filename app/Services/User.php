<?php

namespace App\Services;

use App\Models\User as UserModel;
use App\Repositories\User as UserRepository;

class User
{
    /**
     * @var \App\Repositories\User
     */
    private UserRepository $userRepository;
    
    /**
     * @param \App\Repositories\User $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }
    
    /**
     * @param array $attributes
     *
     * @return \App\Models\User
     */
    public function create(array $attributes): UserModel
    {
        return $this->userRepository->store($attributes);
    }
}