<?php

namespace App\Services;

use App\Entities\Token as TokenEntity;
use App\Repositories\Token as TokenRepository;
use Illuminate\Support\Str;

class Token
{
    /**
     * @var \App\Repositories\Token
     */
    private TokenRepository $tokenRepository;
    
    /**
     * @var \Illuminate\Support\Str
     */
    private Str $strHelper;
    
    /**
     * @param \App\Repositories\Token $tokenRepository
     * @param \Illuminate\Support\Str $strHelper
     */
    public function __construct(TokenRepository $tokenRepository, Str $strHelper)
    {
        $this->tokenRepository = $tokenRepository;
        $this->strHelper = $strHelper;
    }
    
    /**
     * @param array $attributes
     *
     * @return \App\Entities\Token
     */
    public function create(array $attributes): TokenEntity
    {
        $token = $this->strHelper->uuid();
        
        return $this->tokenRepository->store(
            $token,
            $attributes['user_id'],
            $attributes['user_agent'],
            $attributes['user_ip']
        );
    }
}