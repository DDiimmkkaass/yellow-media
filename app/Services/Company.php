<?php

namespace App\Services;

use App\Models\Company as CompanyModel;
use App\Repositories\Company as CompanyRepository;

class Company
{
    /**
     * @var \App\Repositories\Company
     */
    private CompanyRepository $companyRepository;
    
    /**
     * @param \App\Repositories\Company $companyRepository
     */
    public function __construct(CompanyRepository $companyRepository)
    {
        $this->companyRepository = $companyRepository;
    }
    
    /**
     * @param array $attributes
     *
     * @return \App\Models\Company
     */
    public function create(array $attributes): CompanyModel
    {
        return $this->companyRepository->store($attributes);
    }
}