<?php

namespace App\Services;

use App\Models\PasswordReminder as PasswordReminderModel;
use App\Repositories\PasswordReminder as PasswordReminderRepository;
use Carbon\Carbon;
use Illuminate\Support\Str;

class PasswordReminder
{
    /**
     * @var \App\Repositories\PasswordReminder
     */
    private PasswordReminderRepository $passwordReminderRepository;
    
    /**
     * @param \App\Repositories\PasswordReminder $passwordReminderRepository
     */
    public function __construct(PasswordReminderRepository $passwordReminderRepository)
    {
        $this->passwordReminderRepository = $passwordReminderRepository;
    }
    
    /**
     * @param int $userId
     *
     * @return \App\Models\PasswordReminder
     */
    public function create(int $userId): PasswordReminderModel
    {
        $attributes = [
            'user_id' => $userId,
            'code'    => Str::uuid(),
        ];
        
        return $this->passwordReminderRepository->store($attributes);
    }
    
    /**
     * @param \App\Models\PasswordReminder $passwordReminder
     *
     * @return \App\Models\PasswordReminder
     */
    public function complete(PasswordReminderModel $passwordReminder): PasswordReminderModel
    {
        $attributes = [
            'completed'    => true,
            'completed_at' => Carbon::now(),
        ];
        
        $this->passwordReminderRepository->update($passwordReminder, $attributes);
        
        return $passwordReminder;
    }
}