<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'title',
        'phone',
        'description',
    ];
    
    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \App\Models\User|int                  $user
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfUser(Builder $query, User|int $user): Builder
    {
        if ($user instanceof User) {
            return $query->where($this->getTable().'.user_id', $user->id);
        }
        
        return $query->where($this->getTable().'.user_id', $user);
    }
}
