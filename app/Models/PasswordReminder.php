<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class PasswordReminder extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'code',
    ];
    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'code',
    ];
    
    /**
     * @var string[]
     */
    protected $dates = [
        'completed_at',
    ];
    
    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string                                $code
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfCode(Builder $query, string $code): Builder
    {
        return $query->where($this->getTable().'.code', $code);
    }
}
