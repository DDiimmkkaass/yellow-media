<?php

namespace App\Repositories;

use App\Entities\Token as TokenEntity;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;

class Token
{
    /**
     * Token ttl in seconds
     *
     * @var int
     */
    private int $ttl = 900;
    
    /**
     * @param string $token
     * @param int    $userId
     * @param string $userAgent
     * @param string $userIp
     *
     * @return \App\Entities\Token
     */
    public function store(string $token, int $userId, string $userAgent, string $userIp): TokenEntity
    {
        $tokenData = [
            'user_id'    => $userId,
            'user_ip'    => $userIp,
            'user_agent' => $userAgent,
            'expiration' => Carbon::now()->addSeconds($this->ttl)->toJSON(),
        ];
        
        Redis::setex("tokens:$token", $this->ttl, json_encode($tokenData));
        
        $tokenData['token'] = $token;
        
        return new TokenEntity($tokenData);
    }
    
    /**
     * @param string $token
     *
     * @return \App\Entities\Token|null
     */
    public function findByToken(string $token): ?TokenEntity
    {
        $tokenData = Redis::get("tokens:$token");
        
        if (!$tokenData) {
            return null;
        }
        
        $tokenData = @json_decode($tokenData, true);
        
        if (empty($tokenData)) {
            return null;
        }
        
        return new TokenEntity(
            array_merge(
                $tokenData,
                [
                    'token' => $token,
                ]
            )
        );
    }
}