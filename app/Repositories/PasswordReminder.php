<?php

namespace App\Repositories;

use App\Models\PasswordReminder as PasswordReminderModel;

class PasswordReminder
{
    /**
     * @var \App\Models\PasswordReminder
     */
    private PasswordReminderModel $passwordReminderModel;
    
    /**
     * @param \App\Models\PasswordReminder $passwordReminderModel
     */
    public function __construct(PasswordReminderModel $passwordReminderModel)
    {
        $this->passwordReminderModel = $passwordReminderModel;
    }
    
    /**
     * @param array $attributes
     *
     * @return \App\Models\PasswordReminder
     */
    public function store(array $attributes): PasswordReminderModel
    {
        $passwordReminder = new PasswordReminderModel($attributes);
        
        $passwordReminder->save();
        
        return $passwordReminder;
    }
    
    /**
     * @param \App\Models\PasswordReminder $passwordReminder
     * @param array                        $attributes
     *
     * @return \App\Models\PasswordReminder
     */
    public function update(PasswordReminderModel $passwordReminder, array $attributes): PasswordReminderModel
    {
        if (!empty($attributes['completed'])) {
            $passwordReminder->completed = $attributes['completed'];
        }
        
        if (!empty($attributes['completed'])) {
            $passwordReminder->completed_at = $attributes['completed_at'];
        }
        
        if ($passwordReminder->isDirty()) {
            $passwordReminder->save();
        }
        
        return $passwordReminder;
    }
    
    /**
     * @param string $code
     * @param array  $with
     *
     * @return \App\Models\PasswordReminder|null
     */
    public function findByCode(string $code, array $with = []): ?PasswordReminderModel
    {
        return $this->passwordReminderModel->with($with)->ofCode($code)->first();
    }
}