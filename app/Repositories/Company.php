<?php

namespace App\Repositories;

use App\Models\Company as CompanyModel;
use App\Models\User as UserModel;
use Illuminate\Database\Eloquent\Collection;

class Company
{
    /**
     * @var \App\Models\Company
     */
    private CompanyModel $companyModel;
    
    /**
     * @param \App\Models\Company $companyModel
     */
    public function __construct(CompanyModel $companyModel)
    {
        $this->companyModel = $companyModel;
    }
    
    /**
     * @param array $attributes
     *
     * @return \App\Models\Company
     */
    public function store(array $attributes): CompanyModel
    {
        $company = new CompanyModel($attributes);
        
        $company->save();
        
        return $company;
    }
    
    /**
     * @param UserModel|int $user
     * @param array         $with
     *
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public function ofUser(UserModel|int $user, array $with = []): ?Collection
    {
        return $this->companyModel->with($with)->ofUser($user)->get();
    }
}