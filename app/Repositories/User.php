<?php

namespace App\Repositories;

use App\Models\User as UserModel;

class User
{
    /**
     * @var \App\Models\User
     */
    private UserModel $userModel;
    
    /**
     * @param \App\Models\User $userModel
     */
    public function __construct(UserModel $userModel)
    {
        $this->userModel = $userModel;
    }
    
    /**
     * @param array $attributes
     *
     * @return \App\Models\User
     */
    public function store(array $attributes): UserModel
    {
        $user = new UserModel($attributes);
        
        $user->password = $attributes['password'];
        
        $user->save();
        
        return $user;
    }
    
    /**
     * @param \App\Models\User $user
     * @param array            $attributes
     *
     * @return \App\Models\User
     */
    public function update(UserModel $user, array $attributes): UserModel
    {
        if (!empty($attributes['password'])) {
            $user->password = $attributes['password'];
        }
        
        if ($user->isDirty()) {
            $user->save();
        }
        
        return $user;
    }
    
    /**
     * @param int   $userId
     * @param array $with
     *
     * @return \App\Models\User|null
     */
    public function findById(int $userId, array $with = []): ?UserModel
    {
        return $this->userModel->with($with)->find($userId);
    }
    
    /**
     * @param string $email
     * @param array  $with
     *
     * @return \App\Models\User|null
     */
    public function findByEmail(string $email, array $with = []): ?UserModel
    {
        return $this->userModel->with($with)->ofEmail($email)->first();
    }
}