<?php

namespace App\Contracts;

interface RespondTransformer
{
    /**
     * @param mixed $item
     *
     * @return array
     */
    public function transform($item): array;
}