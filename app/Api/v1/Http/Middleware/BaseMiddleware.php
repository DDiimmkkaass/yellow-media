<?php

namespace App\Api\v1\Http\Middleware;

use App\Traits\Responsable;

class BaseMiddleware
{
    use Responsable;
}