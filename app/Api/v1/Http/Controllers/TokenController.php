<?php

namespace App\Api\v1\Http\Controllers;

use App\Api\v1\Http\Requests\Token\Store as StoreRequest;
use App\Api\v1\Http\Transformers\Token as TokenTransformer;
use App\Contracts\RespondTransformer;
use App\Repositories\User as UserRepository;
use App\Services\Token as TokenService;
use Illuminate\Contracts\Auth\Factory;
use Illuminate\Http\JsonResponse;

class TokenController extends BaseController
{
    /**
     * @var \App\Services\Token
     */
    private TokenService $tokenService;
    
    /**
     * @var \App\Repositories\User
     */
    private UserRepository $userRepository;
    
    /**
     * @param \Illuminate\Contracts\Auth\Factory $auth
     * @param \App\Repositories\User             $userRepository
     * @param \App\Services\Token                $tokenService
     */
    public function __construct(Factory $auth, UserRepository $userRepository, TokenService $tokenService)
    {
        parent::__construct($auth);
        
        $this->tokenService = $tokenService;
        $this->userRepository = $userRepository;
    }
    
    /**
     * @return \App\Contracts\RespondTransformer
     */
    protected function transformer(): RespondTransformer
    {
        return new TokenTransformer();
    }
    
    /**
     * @param \App\Api\v1\Http\Requests\Token\Store $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request): JsonResponse
    {
        $user = $this->userRepository->findByEmail($request->email);
        
        return $this->respondWithItem(
            $this->tokenService->create(
                [
                    'user_id'    => $user->id,
                    'user_agent' => $request->userAgent(),
                    'user_ip'    => $request->getClientIp(),
                ]
            )
        );
    }
}
