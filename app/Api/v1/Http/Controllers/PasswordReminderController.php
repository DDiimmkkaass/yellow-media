<?php

namespace App\Api\v1\Http\Controllers;

use App\Api\v1\Http\Requests\PasswordReminder\Complete as CompleteRequest;
use App\Api\v1\Http\Requests\PasswordReminder\Store as StoreRequest;
use App\Api\v1\Http\Transformers\PasswordReminder;
use App\Contracts\RespondTransformer;
use App\Repositories\PasswordReminder as PasswordReminderRepository;
use App\Repositories\User as UserRepository;
use App\Services\PasswordReminder as PasswordReminderService;
use Illuminate\Contracts\Auth\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class PasswordReminderController extends BaseController
{
    /**
     * @var \App\Services\PasswordReminder
     */
    private PasswordReminderService $passwordReminderService;
    
    /**
     * @var \App\Repositories\User
     */
    private UserRepository $userRepository;
    
    /**
     * @var \App\Repositories\PasswordReminder
     */
    private PasswordReminderRepository $passwordReminderRepository;
    
    /**
     * @param \Illuminate\Contracts\Auth\Factory $auth
     * @param \App\Repositories\User             $userRepository
     * @param \App\Repositories\PasswordReminder $passwordReminderRepository
     * @param \App\Services\PasswordReminder     $passwordReminderService
     */
    public function __construct(
        Factory $auth,
        UserRepository $userRepository,
        PasswordReminderRepository $passwordReminderRepository,
        PasswordReminderService $passwordReminderService
    ) {
        parent::__construct($auth);
        
        $this->passwordReminderService = $passwordReminderService;
        $this->userRepository = $userRepository;
        $this->passwordReminderRepository = $passwordReminderRepository;
    }
    
    /**
     * @return \App\Contracts\RespondTransformer
     */
    protected function transformer(): RespondTransformer
    {
        return new PasswordReminder();
    }
    
    /**
     * @param \App\Api\v1\Http\Requests\PasswordReminder\Store $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request): JsonResponse
    {
        $user = $this->userRepository->findByEmail($request->email);
        
        return $this->respondWithItem($this->passwordReminderService->create($user->id));
    }
    
    /**
     * @param \App\Api\v1\Http\Requests\PasswordReminder\Complete $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function complete(CompleteRequest $request): JsonResponse
    {
        $passwordReminder = $this->passwordReminderRepository->findByCode($request->code);
        $user = $this->userRepository->findById($passwordReminder->user_id);
        
        $passwordReminder = DB::transaction(
            function () use ($request, $passwordReminder, $user) {
                $this->userRepository->update(
                    $user,
                    [
                        'password' => $request->password,
                    ]
                );
                
                return $this->passwordReminderService->complete($passwordReminder);
            }
        );
        
        return $this->respondWithItem($passwordReminder);
    }
}
