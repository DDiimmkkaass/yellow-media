<?php

namespace App\Api\v1\Http\Controllers;

use App\Api\v1\Http\Requests\User\Store as StoreRequest;
use App\Services\User as UserService;
use Illuminate\Contracts\Auth\Factory;
use Illuminate\Http\JsonResponse;

class UserController extends BaseController
{
    /**
     * @var \App\Services\User
     */
    private UserService $userService;
    
    /**
     * @param \Illuminate\Contracts\Auth\Factory $auth
     * @param \App\Services\User                 $userService
     */
    public function __construct(Factory $auth, UserService $userService)
    {
        parent::__construct($auth);
        
        $this->userService = $userService;
    }
    
    /**
     * @param \App\Api\v1\Http\Requests\User\Store $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request): JsonResponse
    {
        return $this->respondWithItem($this->userService->create($request->validated()));
    }
}
