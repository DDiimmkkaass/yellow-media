<?php

namespace App\Api\v1\Http\Controllers;

use App\Api\v1\Http\Requests\Company\Store as StoreRequest;
use App\Api\v1\Http\Transformers\Company;
use App\Repositories\Company as CompanyRepository;
use App\Services\Company as CompanyService;
use Illuminate\Contracts\Auth\Factory;
use Illuminate\Http\JsonResponse;

class CompanyController extends BaseController
{
    /**
     * @var \App\Services\Company
     */
    private CompanyService $companyService;
    
    /**
     * @var \App\Repositories\Company
     */
    private CompanyRepository $companyRepository;
    
    /**
     * @param \Illuminate\Contracts\Auth\Factory $auth
     * @param \App\Repositories\Company          $companyRepository
     * @param \App\Services\Company              $companyService
     */
    public function __construct(
        Factory $auth,
        CompanyRepository $companyRepository,
        CompanyService $companyService
    ) {
        parent::__construct($auth);
        
        $this->companyService = $companyService;
        $this->companyRepository = $companyRepository;
    }
    
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->respondWithCollection($this->companyRepository->ofUser($this->user));
    }
    
    /**
     * @param \App\Api\v1\Http\Requests\Company\Store $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request): JsonResponse
    {
        return $this->respondWithItem(
            $this->companyService->create(
                array_merge(
                    $request->validated(),
                    [
                        'user_id' => $this->user->id,
                    ]
                )
            )
        );
    }
}
