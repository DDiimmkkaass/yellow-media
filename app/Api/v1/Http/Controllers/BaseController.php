<?php

namespace App\Api\v1\Http\Controllers;

use Illuminate\Contracts\Auth\Factory;
use Laravel\Lumen\Routing\Controller as Controller;
use App\Traits\Responsable;

class BaseController extends Controller
{
    use Responsable;
    
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    protected $user;
    
    /**
     * @param \Illuminate\Contracts\Auth\Factory $auth
     */
    public function __construct(Factory $auth)
    {
        $this->user = $auth->guard()->user();
    }
}
