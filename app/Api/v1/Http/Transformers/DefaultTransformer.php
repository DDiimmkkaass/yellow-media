<?php

namespace App\Api\v1\Http\Transformers;

use App\Contracts\RespondTransformer;
use Illuminate\Database\Eloquent\Model;

class DefaultTransformer implements RespondTransformer
{
    /**
     * @param mixed $item
     *
     * @return array
     */
    public function transform($item): array
    {
        if (is_array($item)) {
            return $item;
        }
        
        if ($item instanceof Model) {
            return $item->toArray();
        }
        
        return [
            $item,
        ];
    }
}