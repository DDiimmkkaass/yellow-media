<?php

namespace App\Api\v1\Http\Transformers;

use App\Contracts\RespondTransformer;
use App\Entities\Token as TokenEntity;

class Token implements RespondTransformer
{
    /**
     * @param TokenEntity $item
     *
     * @return array
     */
    public function transform($item): array
    {
        return [
            'token'      => $item->token,
            'expired_at' => $item->expiredAt,
        ];
    }
}