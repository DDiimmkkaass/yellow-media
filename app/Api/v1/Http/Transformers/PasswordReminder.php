<?php

namespace App\Api\v1\Http\Transformers;

use App\Contracts\RespondTransformer;
use App\Models\PasswordReminder as PasswordReminderModel;

class PasswordReminder implements RespondTransformer
{
    /**
     * @param PasswordReminderModel $item
     *
     * @return array
     */
    public function transform($item): array
    {
        $data = $item->toArray();
        
        $data['code'] = $item->code.' Just for a test!!!';
        
        return $data;
    }
}