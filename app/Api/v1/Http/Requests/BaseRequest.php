<?php

namespace App\Api\v1\Http\Requests;

use App\Traits\Responsable;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Pearl\RequestValidate\RequestAbstract;

class BaseRequest extends RequestAbstract
{
    use Responsable;
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }
    
    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages(): array
    {
        return [];
    }
    
    /**
     * @param \Illuminate\Contracts\Validation\Validator $validator
     *
     * @return \Illuminate\Validation\ValidationException
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator): ValidationException
    {
        throw new ValidationException(
            $validator,
            $this->respondWithError(
                422,
                'Validation error',
                $validator->getMessageBag()->getMessages()
            )
        );
    }
}