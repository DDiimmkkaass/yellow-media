<?php

namespace App\Api\v1\Http\Requests\PasswordReminder;

use App\Api\v1\Http\Requests\BaseRequest;
use Illuminate\Validation\Rules\Password;

class Complete extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'code'     => [
                'required',
                'string',
                'actual_code',
            ],
            'password' => [
                'required',
                'confirmed',
                Password::min(8),
            ],
        ];
    }
}