<?php

namespace App\Api\v1\Http\Requests\PasswordReminder;

use App\Api\v1\Http\Requests\BaseRequest;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class Store extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => [
                'required',
                'email',
                Rule::exists('users'),
            ],
        ];
    }
    
    /**
     * @return void
     */
    public function prepareForValidation()
    {
        $this->merge(
            [
                'email' => (string)filter_var(Str::lower($this->input('email')), FILTER_VALIDATE_EMAIL),
            ]
        );
    }
}