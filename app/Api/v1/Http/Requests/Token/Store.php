<?php

namespace App\Api\v1\Http\Requests\Token;

use App\Api\v1\Http\Requests\BaseRequest;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class Store extends BaseRequest
{
    /**
     * @var string
     */
    private string $email;
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email'    => [
                'required',
                'email',
                Rule::exists('users'),
            ],
            'password' => [
                'required',
                'string',
                'user_password:'.$this->email,
            ],
        ];
    }
    
    /**
     * @return void
     */
    public function prepareForValidation()
    {
        $this->email = (string)filter_var(Str::lower($this->input('email')), FILTER_VALIDATE_EMAIL);
        
        $this->merge(
            [
                'email' => $this->email,
            ]
        );
    }
}