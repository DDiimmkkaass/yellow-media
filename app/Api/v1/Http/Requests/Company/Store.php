<?php

namespace App\Api\v1\Http\Requests\Company;

use App\Api\v1\Http\Requests\BaseRequest;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class Store extends BaseRequest
{
    /**
     * @var int
     */
    private int $userId;
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title'       => [
                'required',
                'string',
                'min:2',
                'max:255',
                Rule::unique('companies')
                    ->where('companies.user_id', $this->userId),
            ],
            'description' => 'nullable|string|max:1500',
            'phone'       => [
                'nullable',
                'regex:/^380[0-9]{9}$/',
                Rule::unique('companies')
                    ->where('companies.user_id', $this->userId),
            ],
        ];
    }
    
    /**
     * @return void
     */
    public function prepareForValidation()
    {
        $this->userId = $this->user()->id;
        
        $this->merge(
            [
                // just for the postman randomPhoneNumber support (xxx-xxx-xxxx)
                'phone' => '380'.Str::limit(Str::remove('-', $this->input('phone')), 9, ''),
            ]
        );
    }
}