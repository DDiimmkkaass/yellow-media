<?php

namespace App\Api\v1\Http\Requests\User;

use App\Api\v1\Http\Requests\BaseRequest;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;

class Store extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'first_name' => 'required|string|min:2|max:255',
            'last_name'  => 'required|string|min:2|max:255',
            'email'      => [
                'required',
                'email',
                Rule::unique('users'),
            ],
            'phone'      => [
                'required',
                'regex:/^380[0-9]{9}$/',
                Rule::unique('users'),
            ],
            'password'   => [
                'required',
                'confirmed',
                Password::min(8),
            ],
        ];
    }
    
    /**
     * @return void
     */
    public function prepareForValidation()
    {
        $this->merge(
            [
                'email' => Str::lower($this->input('email')),
                
                // just for the postman randomPhoneNumber support (xxx-xxx-xxxx)
                'phone' => '380'.Str::limit(Str::remove('-', $this->input('phone')), 9, ''),
            ]
        );
    }
}