<?php

namespace App\Providers;

use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
    
    /**
     * Boot the authentication services for the application.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function boot(Request $request)
    {
        $apiVersion = $this->app->make('request')->headers->get('api-version', 'v1');
        
        $this->app->router->group([
            'namespace' => "App\Api\\$apiVersion\Http\Controllers",
            'prefix'    => 'api',
        ], function ($router) use ($apiVersion) {
            require $this->app->basePath("routes/$apiVersion/api.php");
        });
    }
}
