<?php

namespace App\Providers;

use App\Repositories\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use InvalidArgumentException;

class ValidationServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->validateUserPassword();
    }
    
    /**
     * @return void
     */
    protected function validateUserPassword()
    {
        Validator::extend(
            'user_password',
            function ($attribute, $value, $parameters, $validator) {
                if (count($parameters) < 1) {
                    throw new InvalidArgumentException("Validation rule user_password requires an email of user as a parameter.");
                }
                
                $user = $this->app->make(User::class)->findByEmail($parameters[0]);
                
                if (!$user) {
                    return false;
                }
                
                $hasher = $this->app->make('hash');
                
                return $hasher->check($value, $user->getAuthPassword());
            }
        );
        
        Validator::extend(
            'actual_code',
            function ($attribute, $value, $parameters, $validator) {
                return (bool)DB::table('password_reminders')
                    ->leftJoin('users', 'users.id', '=', 'password_reminders.user_id')
                    ->where('password_reminders.code', $value)
                    ->where('password_reminders.completed', false)
                    ->count();
            }
        );
    }
}
