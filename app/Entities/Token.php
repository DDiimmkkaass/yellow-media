<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Support\Arr as ArrHelper;
use Illuminate\Support\Str as StrHelper;

/**
 * @property-read int            $userId
 * @property-read string         $token
 * @property-read string         $userAgent
 * @property-read string         $ipAddress
 * @property-read \Carbon\Carbon $expiredAt
 */
class Token
{
    /**
     * @var array
     */
    private array $attributes;
    
    /**
     * @param array $attributes
     */
    public function __construct(array $attributes)
    {
        $this->attributes = $attributes;
    }
    
    /**
     * @param $name
     *
     * @return mixed
     */
    public function __get($name): mixed
    {
        if (is_callable([$this, $name])) {
            return $this->{$name}();
        }
        
        return ArrHelper::get($this->attributes, StrHelper::snake($name));
    }
    
    /**
     * @return \Carbon\Carbon
     */
    public function expiredAt(): Carbon
    {
        return Carbon::parse(ArrHelper::get($this->attributes, 'expired_at'));
    }
}