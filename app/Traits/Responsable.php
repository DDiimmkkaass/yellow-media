<?php

namespace App\Traits;

use App\Api\v1\Http\Transformers\DefaultTransformer;
use App\Contracts\RespondTransformer as TransformerContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;

trait Responsable
{
    /**
     * @var string
     */
    protected string $dataKey = 'data';
    
    /**
     * Http status code of response
     *
     * @var int
     */
    protected int $httpCode = 200;
    
    /**
     * @return \App\Contracts\RespondTransformer
     */
    protected function transformer(): TransformerContract
    {
        return new DefaultTransformer();
    }
    
    /**
     * @param int $httpCode
     *
     * @return \App\Traits\Responsable
     */
    protected function setStatusCode(int $httpCode): static
    {
        $this->httpCode = $httpCode;
        
        return $this;
    }
    
    /**
     * @param mixed    $item
     * @param int|null $httpCode
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithItem(mixed $item, ?int $httpCode = null): JsonResponse
    {
        return $this->respondWithArray(
            [
                $this->dataKey => $this->transformer()->transform($item),
                'meta'         => [],
            ],
            $httpCode
        );
    }
    
    /**
     * @param \Illuminate\Support\Collection $items
     * @param int|null                       $total
     * @param int|null                       $httpCode
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithCollection(
        Collection $items,
        int|null $total = null,
        ?int $httpCode = null
    ): JsonResponse {
        return $this->respondWithArray(
            [
                $this->dataKey => $items->map(fn(Model $item) => $this->transformer()->transform($item))->toArray(),
                'meta'         => [
                    'total' => $total ?: $items->count(),
                ],
            ],
            $httpCode
        );
    }
    
    /**
     * @param string $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondUnauthorized(string $message = 'Unauthorized'): JsonResponse
    {
        return $this->respondWithError(401, $message);
    }
    
    /**
     * @param array    $data
     * @param int|null $httpCode
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithArray(array $data, ?int $httpCode = null): JsonResponse
    {
        return response()->json($data, $httpCode ?? $this->httpCode);
    }
    
    /**
     * @param int         $httpCode
     * @param string|null $message
     * @param array|null  $validation
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithError(
        int $httpCode = 400,
        ?string $message = null,
        ?array $validation = null
    ): JsonResponse {
        $error = [
            'error' => [
                'message' => $message ? : 'Unresolved error',
            ],
        ];
        
        if (!empty($validation)) {
            $error['error']['validation'] = $validation;
        }
        
        return $this->respondWithArray(
            $error,
            $httpCode
        );
    }
}