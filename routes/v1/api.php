<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(
    [
        'prefix' => 'users',
    ],
    function () use ($router) {
        $router->post('/', ['uses' => 'UserController@store']);
        
        $router->group(
            [
                'prefix' => 'passwords',
            ],
            function () use ($router) {
                $router->group(
                    [
                        'prefix' => 'reminders',
                    ],
                    function () use ($router) {
                        $router->post('/', ['uses' => 'PasswordReminderController@store']);
                        
                        $router->patch('/', ['uses' => 'PasswordReminderController@complete']);
                    }
                );
            }
        );
        
        $router->group(
            [
                'prefix'     => 'companies',
                'middleware' => ['auth'],
            ],
            function () use ($router) {
                $router->get('/', ['uses' => 'CompanyController@index']);
                
                $router->post('/', ['uses' => 'CompanyController@store']);
            }
        );
    }
);

$router->group(
    [
        'prefix' => 'tokens',
    ],
    function () use ($router) {
        $router->post('/', ['uses' => 'TokenController@store']);
    }
);
